import { LayoutOptions, Layout, FData, Control, Form, FormCallbacks } from './index.ts'

const defaultLayoutOpts: LayoutOptions = {
  when: () => true
}

type CtrlFn = (name: string) => Control|null

function buildLayout(
  controls: (data: FData) => Control[],
  layoutOpts: LayoutOptions,
  render: (data: FData, getControl: CtrlFn, formCbs: FormCallbacks) => Node,
  update: (data: FData, current: Node) => void,
  getVisibleValues: (allFormData: FData) => FData
): Layout {

  let _domElem: Node|null = null

  return {
    controls,

    shouldRender(data: FData): boolean {
      return (layoutOpts.when || defaultLayoutOpts.when)(data)
    },

    render,

    update,

    hide(parent: Node) {
      if (!_domElem) {
        return
      }
      for (const n of parent.childNodes) {
        if (n === _domElem) {
          parent.removeChild(n)
        }
      }
    },

    values: getVisibleValues
  }
}

export function field(name: string, options?: LayoutOptions): Layout {
  let controlsArr: Control[] = []
  function render(data: FData, getControl: CtrlFn, formCbs: FormCallbacks): Node {
    const c: Control|null = getControl(name)
    if (c) {
      controlsArr.push(c)
      return c.render(data, formCbs)
    } else {
      return document.createDocumentFragment()
    }
  }

  function update(data: FData, current: Node) {}

  function controls(): Control[] {
    return controlsArr
  }

  const opts = options || {}

  function getVisibleValues(allFormData: FData): FData {
    const shouldRender = opts.when ? opts.when(allFormData.data) : true
    const r: FData = {}
    if (shouldRender) {
      for (const c of controlsArr) {
        if (c.hasValue()) {
          r[c.name] = c.getValue()
        }
      }
    }
    return r
  }

  return buildLayout(controls, opts, render, update, getVisibleValues)
}

export function header(content: string|Node, options?: LayoutOptions): Layout {
  function render(): Node {
    const el = document.createElement('p')
    let child: Node
    if (typeof content === 'string') {
      child = document.createTextNode(content)
    } else {
      child = content
    }
    el.appendChild(child)
    return el;
  }

  // noop
  function update() {}

  function getVisibleValues(allFormData: FData): FData {
    return {}
  }


  const opts = options || {}
  return buildLayout(() => [], opts, render, update, getVisibleValues)
}

export function vertical(elems: Layout[], options?: LayoutOptions): Layout {
  const nodes: {[idx: number]: {node: Node, attached: boolean}} = {}

  function render(data: FData, getControl: CtrlFn, formCbs: FormCallbacks): Node {
    const container = document.createElement('div')
    for (const i in elems) {
      const le = elems[i]
      // render anyway, but only attach elements that should be rendered
      const node = le.render(data, getControl, formCbs)
      if (le.shouldRender(data)) {
        container.appendChild(node)
        nodes[i] = {node, attached: true}
      } else {
        nodes[i] = {node, attached: false}
      }
    }
    return container
  }

  function update(data: FData, currentEl: Node) {
    for (const i in elems) {
      const le = elems[i]
      const { node, attached } = nodes[i]
      if (le.shouldRender(data)) {
        le.update(data, node)
        // if it was not attached yet, attach it
        if (!attached) {
          currentEl.appendChild(node)
          nodes[i] = { node, attached: true }
        }
      } else {
        // hide element if currently shown
        try {
          currentEl.removeChild(node)
        } catch (ignore) {}
        nodes[i] = { node, attached: false }
      }
    }
  }

  function controls(data: FData): Control[] {
    let cs: Control[] = []
    for (const le of elems) {
      if (le.shouldRender(data)) {
        for (const c of le.controls(data)) {
          cs.push(c)
        }
      }
    }
    return cs
  }

  const opts = options || {}

  function getVisibleValues(allFormData: FData): FData {
    const shouldRender = opts.when ? opts.when(allFormData.data) : true
    const r: FData = {}
    if (shouldRender) {
      for (const c of controls(allFormData)) {
        if (c.hasValue()) {
          r[c.name] = c.getValue()
        }
      }
    }
    return r
  }

  return buildLayout(controls, opts, render, update, getVisibleValues)
}
