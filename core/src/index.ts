
export interface Control {
  // the name of the control, used as key in the resulting form data object
  name: string

  // whether the given value is valid or not
  validator: Validator

  // whether the control can provide some value to the form data object.
  // this is often `true`, but can be false, e.g. when a submit button is excplicitly excluded,
  // or when the element is hidden from the dom because of some context.
  // Note: an invalid value is not a reason for not providing data!
  hasValue(): boolean

  // the value provided to the form data object
  getValue(): any

  // renders the control into the given `parent`
  render(formData: FData, formCbs: FormCallbacks): Node

}

export interface Layout {
  // returns all the controls managed by this layout
  controls(data: FData): Control[]

  // whether the element thinks it is visible
  shouldRender(data: FData): boolean

  // renders the control into the given `parent`
  render(
    data: FData,
    getControl: (name: string) => Control|null,
    formCb: FormCallbacks): Node

  // if the component has rendered already, update it (if needed)
  update(data: FData, current: Node): void

  values(data: FData): FData

  // hide the control from the DOM
  hide(parent: Node): void

}

export interface LayoutOptions {
  when?: (data: FData) => boolean
}

export interface FData {
  // A key string for each control that provides a value, and a value.
  // Note: a control can produce any kind of value.
  [name:string]: any
}

// TODO
export interface Spec {
  controls: Control[]
  layout: Layout
}

export interface ValidationResult {
  isValid: boolean
  errors: string[]
}

// async, because validation may need to ask remote server
export type Validator = (value: any, allData: FData) => Promise<ValidationResult>

export function buildValidator(v: (value: any) => string[]): Validator {
  return function(value: any, allData: FData): Promise<ValidationResult> {
      return new Promise((resolve) => {
        const errors = v(value)
        resolve({
          isValid: errors.length === 0,
          errors
        })
      })
    }
}

export interface FormCallbacks {
  updateValue(name: string, newValue: any): void
}

export class Form implements FormCallbacks {

  private _renderedNode: Node | null = null

  private _layout: Layout

  // note: the source of truth is the controls itself. This may not be the absolute
  // latest values. To get the latest values, run `this._layout.values(this._latestData)`
  private _latestData: FData

  constructor(public spec: Spec, private initialData: object) {
    this._layout = spec.layout
    this._latestData = initialData
  }

  private _getControl: (name: string) => Control|null = (name) => {
    for (const c of this.spec.controls) {
      if (c.name === name) {
        return c
      }
    }
    return null
  }

  controls(): Control[] {
    return this._layout.controls(this)
  }

  // this should be called by the controls after data has changed
  updateValue(name: string, newValue: any) {
    console.log('updated field ' + name, 'to:',  newValue)
    if (this._renderedNode) {
      // get all values, then update layout
      const xs = {...this._latestData}
      xs[name] = newValue
      const vs = this._layout.values(xs)
      this._latestData = vs

      // TODO layout.update should return info about changes to visible state,
      // TODO so that we know if we have to update again, with new values.
      // TODO This must be done recursively, until no changes to the set of visible controls
      // TODO have been made.
      this._layout.update(vs, this._renderedNode)
    }
    console.log('data:', this._latestData)
  }

  // render the form
  render(parent: Node) {
    const f = document.createElement('form')
    const layout = this._layout
    if (layout.shouldRender(this.initialData)) {
      const callback: FormCallbacks = this
      this._renderedNode = layout.render(this.initialData, this._getControl, callback)
      f.appendChild(this._renderedNode)
    }
    parent.appendChild(f)
  }
}
