import { Validator, ValidationResult, buildValidator, Form, FormCallbacks, Control, FData } from '../../core/src/index'

export type Listener = (form: Form, ev: Event) => void

export interface BasicOptions {
  name: string
  label?: string
  validator?: Validator
  excludeData?: boolean
  listener?: {
    [evType:string]: Listener
  }
}

const defaultValidator: Validator = () => Promise.resolve({isValid: true, errors: []})

const defaultOpts = {
  validator: defaultValidator,
  excludeData: false,
  listener: {}
}

// meant to be used as part of an implementation of `Control`
class BasicControl<Opts extends BasicOptions> {

  _options: Opts

  validator: Validator

  name: string

  constructor(options: Opts) {
    this._options = {...defaultOpts, ...options}
    this.validator = this._options.validator
    this.name = options.name
  }

  validate(allData: FData): Promise<ValidationResult> {
    return this.validator(this.getValue(), allData)
  }

  hasValue() {
    return !this._options.excludeData
  }

  // implement this in subclass
  // TODO better design. This bypasses compiler checks, which is bad
  getValue(): any {
    throw new Error('getValue() is not implemented in subclass')
  }

}

export interface TextInputOptions extends BasicOptions {
  type?: string
  placeholder?: string
}


export class TextInput extends BasicControl<TextInputOptions> implements Control {

  private _domElem: HTMLInputElement | null = null

  constructor(options: TextInputOptions) {
    super(options)
  }

  getValue() {
    return this._domElem ? this._domElem.value : null
  }

  render(data: FData, formCbs: FormCallbacks): Node {
    const name = this._options.name
    const initValue: string = data[name] && typeof data[name] === 'string' ? data[name] : ''
    const div = document.createElement('div')

    const input = document.createElement('input')
    input.value = initValue
    input.addEventListener('change', () => {
      formCbs.updateValue(this._options.name, input.value)
    })
    if (this._options.placeholder) {
      input.setAttribute('placeholder', this._options.placeholder)
    }
    input.setAttribute('type', this._options.type ? this._options.type : 'text')
    input.setAttribute('name', name)
    this._domElem = input

    if (this._options.label) {
      // TODO add `for` and `id` to relate label to input
      const label = document.createElement('label')
      label.appendChild(document.createTextNode(this._options.label))
      div.appendChild(label)
    }

    div.appendChild(input)
    return div
  }
}

export class CheckboxInput extends BasicControl<BasicOptions> implements Control {

  private _domElem: HTMLInputElement | null = null

  constructor(options: BasicOptions) {
    super(options)
  }

  getValue() {
    return this._domElem ? this._domElem.checked : null
  }

  render(data: FData, formCbs: FormCallbacks): Node {
    const name = this._options.name
    const initValue: boolean = data[name] && typeof data[name] === 'boolean' ? data[name] : false
    const div = document.createElement('div')

    const input = document.createElement('input')
    input.setAttribute('type', 'checkbox')
    input.setAttribute('name', name)
    input.checked = initValue
    input.addEventListener('change', () => {
      formCbs.updateValue(name, input.checked)
    })

    this._domElem = input

    div.appendChild(input)

    if (this._options.label) {
      // TODO add `for` and `id` to relate label to input
      const label = document.createElement('label')
      label.appendChild(document.createTextNode(this._options.label))
      div.appendChild(label)
    }

    return div
  }

}
