const path = require('path');

module.exports = {
  mode: 'none',
  entry: {
    core: './core/src/index.ts',
    controls: './controls/src/index.ts',
    demo: './index.ts'
  },
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'dom-forms-[name].js'
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'ts-loader',
            options: {onlyCompileBundledFiles: true}
          }
        ],
        exclude: [/node_modules/]
      }
    ]
  }
};

