// example js
import * as core from './core/src/index.ts'
import * as layout from './core/src/layout.ts'
import * as controls from './controls/src/index.ts'

document.body.appendChild(document.createTextNode('ho'))

const spec: core.Spec = {
  controls: [
    new controls.TextInput({
      name: 'username',
      label: 'Username',
      placeholder: 'your username'
    }),
    new controls.CheckboxInput({
      name: 'toggle',
      label: 'Toggle'
    })
  ],
  layout: layout.vertical([
    layout.header('test header'),
    layout.field('toggle'),
    layout.field('username', {when: (data: core.FData) => data['toggle']}),
    layout.header('2nd header')
  ])
}
const form = new core.Form(spec, {'username': 'martin meier', 'toggle': true})

form.render(document.getElementById('demo'))

