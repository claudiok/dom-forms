import { Spec, Form, FData, Control } from './core/src/index.ts';
import * as yup from 'yup'; // for validation

// TODO
const controls: any = null

// declaration of form
const spec: Spec = {
  fields: {
    name: controls.textInput({
      name: 'name',
      label: 'Name',
      type: 'text',
      placeholder: 'Your name',
      validation: yup.string().min(2).max(100).required(),
      listener: {
        change: function(form, ev) { console.log(e) }
      },
    }),
    alive: controls.checkbox({
      name: 'alive',
      label: 'still alive?',
      validation: null,
      listener: {},
    }),
    death: controls.singleSelect({
      name: 'type-of-death',
      label: 'Type of death',
      options: {
        '-': 'please choose...',
        'vampire': 'Vampire',
        'zombie': 'Zombie'
      },
      validation: yup.oneOf(['vampire', 'zombie']).required()
    }),
    submit: controls.button({
      name: 'submit',
      label: 'Submit',
      excludeData: true, // exclude the value of this submit button in the resulting data object
      value: 'submit',
      listener: {
        submit: (form) => { console.log(form.isValid, form.errors, form.data) }
      }
    })
  },
  layout: f.layout.vertical(
      elems: [
        f.layout.header('Example form'),
        f.field('name'),
        f.field('alive'),
        f.field('death', { when: (data) => { !data['alive'] } })
      ],
      { when: (data) => true }
    )
};

const initialData = { name: 'John Doe' }
const form = new Form(spec, initialData)

f.renderForm(document.getElementById('form'), form);

